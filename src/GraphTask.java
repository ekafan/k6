import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (10, 15);

      long time = System.currentTimeMillis();

      g.findMIS();

      long time2 = System.currentTimeMillis() - time;

      System.out.println("Current Graph: Vertices = 10; Arcs = 15");
      System.out.println("Running time is " + time2 + " ms");


       g.createRandomSimpleGraph (100, 150);

       long time3 = System.currentTimeMillis();

       g.findMIS();

       long time4 = System.currentTimeMillis() - time3;

       System.out.println("Current Graph: Vertices = 100; Arcs = 150");
       System.out.println("Running time is " + time4 + " ms");


       g.createRandomSimpleGraph (1000, 1500);

       long time5 = System.currentTimeMillis();

       g.findMIS();

       long time6 = System.currentTimeMillis() - time5;

       System.out.println("Current Graph: Vertices = 1000; Arcs = 1500");
       System.out.println("Running time is " + time6 + " ms");


       g.createRandomSimpleGraph (2300, 3500);

       long time7 = System.currentTimeMillis();

       g.findMIS();

       long time8 = System.currentTimeMillis() - time7;

       System.out.println("Current Graph: Vertices = 2300; Arcs = 3500");
       System.out.println("Running time is " + time8 + " ms");
   }

   /**
    * Current function creates an undirected Graph G and finds
    * Maximal Independent Set (MIS).
    * All Vertices in MIS has no Arc with any other from the MIS.
    * If we add any additional Vertex to MIS, then it would not be independent any more.
    */

   class Vertex implements Comparable<Vertex> {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int arcCount = 0;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

       @Override
       public int compareTo(Vertex other) {
          if (arcCount > other.arcCount) {
              return 1;
          } else if (arcCount < other.arcCount) {
              return -1;
          }
          return 0;
       }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      private LinkedList<Vertex> vertexLinkedList = new LinkedList<>();

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;

         from.arcCount++;

         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Create a list of all Vertices
       * Choose smallest from the list
       * Add Vertex to a new list and remove all its neighbours
       * @return Maximal Independent Set (MIS)
       */
      public ArrayList<Vertex> findMIS() {
         // to create biggest possible (maximal) MIS, we need to start from Vertices with smallest count of Arcs

          Vertex v = first;
          while (v != null) {

              vertexLinkedList.add(v); // create a list of all Vertices in Graph
              v = v.next;
          }

          Collections.sort(vertexLinkedList); // Vertices with small Arc count to the beginning

          ArrayList<Vertex> mis = new ArrayList<>(); // list for MIS

          while (vertexLinkedList.size() > 0) {
              Vertex base = vertexLinkedList.pop();
              mis.add(base); // add current vertex to MIS

              Arc a = base.first;
              while (a != null) {

                  vertexLinkedList.remove(a.target); // remove all adjacent Vertices

                  a = a.next;
              }
          }

         return mis;
      }
   }

} 

