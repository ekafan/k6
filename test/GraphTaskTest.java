import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void test1() {
      GraphTask main = new GraphTask();
      GraphTask.Graph g = main.new Graph("G");

      g.createRandomSimpleGraph(6, 10);
      System.out.println(g);
      System.out.println(g.findMIS());
   }

}

